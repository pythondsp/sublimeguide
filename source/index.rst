.. Sublime text-editor documentation master file, created by
   sphinx-quickstart on Sat Oct  7 17:38:14 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sublime text-editor
===================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   sublime

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
