Sublime-text-editor
*******************

Installation and packages
=========================
    
    * Run below command in Ubuntu/Mint or download executable file from the windows. Also, portable version of sublime is available, which can be saved and executed in usb-stick. 

    .. code-block:: shell

        sudo add-apt-repository ppa:webupd8team/sublime-text-3 && sudo apt-get update && sudo apt-get install sublime-text-installer

    * After installing sublime, press **ctrl+shift+p** and select "**Install Packages Control**"

    * Install packages: **ctrl+shift+p** and select **Package Control: Install Packages**, then **type the package name**
            * print to html, 
            * reStructuredText Improved, 
            * reStructured Text (RST) snippests.
            * Cython syntax support
            * VHDL package for sublime text 
            * SystemVerilog


    * Change tabs-to-spaces : go to "preferences->settings" and replace the existing code with  below code,
     
        .. code-block:: shell
           
           {
            "ignored_packages":
            [
                "RestructuredText",
                "Vintage"
            ],

            "translate_tabs_to_spaces": true,
           }

    * Convert existing 'tabs' to 'spaces : go to "preferences->Key Bindings" and replace the existing code with
    
        .. code-block:: shell
        
            [
                    { "keys": ["ctrl+shift+y"], "command": "expand_tabs", "args" : {"set_translate_tabs" : true} }
            ]

        Now press 'ctrl+shift+y' in the file,  to replace tabs with spaces in the existing files. 
        
    * List of various software for Linux are shown at `Linux software list <http://linux-software-list.readthedocs.io/en/latest/index.html>`_
    
    
Keyboard shortcuts
==================


Editing
-------


+------------------------------+----------------------------------------------------------+
| Command                      | Description                                              |
+==============================+==========================================================+
| ctrl kk                      | delete line                                              |
+------------------------------+----------------------------------------------------------+
| ctrl k backspace             | delete from cursor to end of line                        |
+------------------------------+----------------------------------------------------------+
| ctrl shift k                 | delete line                                              |
+------------------------------+----------------------------------------------------------+
| ctrl x                       | cut line                                                 |
+------------------------------+----------------------------------------------------------+
| ctrl shift d                 | duplicate line                                           |
+------------------------------+----------------------------------------------------------+
| ctrl j                       | join below line to current line                          |
+------------------------------+----------------------------------------------------------+
| ctrl shift up-arrow          | move line/selection up                                   |
+------------------------------+----------------------------------------------------------+
| ctrl shift down-arrow        | move line/selection down                                 |
+------------------------------+----------------------------------------------------------+
| ctrl d                       | select word, press again to select next occurrence       |
+------------------------------+----------------------------------------------------------+
| ctrl l                       | select line, press again to select next line             |
+------------------------------+----------------------------------------------------------+
| ctrl enter                   | insert blank line below                                  |
+------------------------------+----------------------------------------------------------+
| ctrl shift enter             | insert blank line above                                  |
+------------------------------+----------------------------------------------------------+
| ctrl m                       | go to end of parentheses; press again to go to beginning |
+------------------------------+----------------------------------------------------------+
| ctrl shift m                 | select all content of current parentheses                |
+------------------------------+----------------------------------------------------------+
| ctrl ]                       | indent line                                              |
+------------------------------+----------------------------------------------------------+
| ctrl [                       | unindent line                                            |
+------------------------------+----------------------------------------------------------+
| ctrl shift d                 | duplicate line                                           |
+------------------------------+----------------------------------------------------------+
| ctrl /                       | comment/uncomment line                                   |
+------------------------------+----------------------------------------------------------+
| ctrl shift /                 | block-comment the selection e.g. in C and C++            |
+------------------------------+----------------------------------------------------------+
| ctrl y                       | redo the last operation/command                          |
+------------------------------+----------------------------------------------------------+
| ctrl shift v                 | paste and indent correctly                               |
+------------------------------+----------------------------------------------------------+
| ctrl shift f                 | find and replace in open/selected files/folders          |
+------------------------------+----------------------------------------------------------+
| ctrl h                       | replace                                                  |
+------------------------------+----------------------------------------------------------+
| ctrl f                       | find                                                     |
+------------------------------+----------------------------------------------------------+
| ctrl o                       | open file                                                |
+------------------------------+----------------------------------------------------------+
| ctrl n                       | new file                                                 |
+------------------------------+----------------------------------------------------------+
| alt shift p                  | print to html                                            |
+------------------------------+----------------------------------------------------------+
| ctrl shift right-mouse-click | select beginning/end of multiple lines                   |
+------------------------------+----------------------------------------------------------+
| ctrl +                       | zoom in                                                  |
+------------------------------+----------------------------------------------------------+
| ctrl -                       | zoom out (will not work for .rst file)                   |
+------------------------------+----------------------------------------------------------+

Navigation
----------

+--------------------+----------------------------------+
| Command            | Description                      |
+====================+==================================+
| ctrl g             | go to line number                |
+--------------------+----------------------------------+
| ctrl ;             | go to first occurrence of word   |
+--------------------+----------------------------------+
| ctrl r             | go to symbol                     |
+--------------------+----------------------------------+
| ctrl p             | open file with name              |
+--------------------+----------------------------------+
| ctrl kb            | toggle sidebar                   |
+--------------------+----------------------------------+
| ctrl up/down-arrow | move screen up/down (not cursor) |
+--------------------+----------------------------------+



Tabs
----

+---------------+-----------------------------+
| Command       | Description                 |
+===============+=============================+
| ctrl PageUp   | go to next tab (cyclic)     |
+---------------+-----------------------------+
| ctrl PageDown | go to previous tab (cyclic) |
+---------------+-----------------------------+
| ctrl w        | close current tab           |
+---------------+-----------------------------+
| ctrl shift t  | open last closed tab        |
+---------------+-----------------------------+


Split windows
-------------

+------------------------+-----------------------------------+
| Command                | Description                       |
+========================+===================================+
| alt shift 1            | single screen                     |
+------------------------+-----------------------------------+
| alt shift 2            | split view in 2 columns           |
+------------------------+-----------------------------------+
| alt shift 3            | split view in 3 columns           |
+------------------------+-----------------------------------+
| alt shift 4            | split view in 4 columns           |
+------------------------+-----------------------------------+
| alt shift 5            | split in grid-view with 4 columns |
+------------------------+-----------------------------------+
| alt shift 8            | split in grid-view with 2 row     |
+------------------------+-----------------------------------+
| alt shift 9            | split in grid-view with 3 row     |
+------------------------+-----------------------------------+
| ctrl number(1-4)       | go to screen-number               |
+------------------------+-----------------------------------+
| ctrl shift number(1-4) | move file to screen-number        |
+------------------------+-----------------------------------+



Bookmark
--------

+---------------+-------------------------+
| Command       | Description             |
+===============+=========================+
| ctrl F2       | Toggle Bookmark         |
+---------------+-------------------------+
| F2            | go to next bookmark     |
+---------------+-------------------------+
| shift F2      | go to previous bookmark |
+---------------+-------------------------+
| ctrl shift F2 | clear all bookmarks     |
+---------------+-------------------------+

Text cases
----------

Put the cursor below the word, 

+---------+-------------+
| Command | Description |
+=========+=============+
| ctrl KL | lowercase   |
+---------+-------------+
| ctrl KU | uppercase   |
+---------+-------------+


Auto-completion
---------------

* Type part of spelling and press tab to complete it. 
* Also, we can type first few and last few letters of the spelling to select the word with similar letters. For example to auto-complete state_reg, we can type sr and then press tab. 


Spell check
-----------

F6 : turn on/off spell checking


RestructuredText commands
-------------------------

* rst table : ctrl t enter
* underline the heading : type symbol three or more times, and press tab
* Go to next header : alt up/down-arrow